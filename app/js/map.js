const mapSmall  = document.getElementById('map-display');
const mapList   = document.getElementById('map-listing');
const modalMap  = document.getElementById('modal-hotel-map');
const totalArea = $(modalMap).find('.list-item-total')[0];
const listArea  = $('#list-item');

const data = {
    count: 3,
    available: 2,
    zoom: 13,
    hotels: [
        {
            hco_hotel_id: "15",
            hco_category_id: "1",
            hco_id: "12",
            hco_latitude: "21.016388",
            hco_longitude: "105.783442",
            hco_name: "InterContinental Hanoi Landmark72",
            hco_line: "Keangnam Hanoi Landmark Tower , Plot E6",
            hco_rating: "5",
            hco_json_amenities: [24, 4454, 2074, 2168],
            hco_statistics: {
                52: {id: "52", name: "Total number of rooms - 358", value: "358"},
                54: {id: "54", name: "Number of floors - 10", value: "10"},
            },
            discount: "30%",
            image: 'https://images.unsplash.com/photo-1615266895738-11f1371cd7e5?ixid=MXwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyMXx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'
        },
        {
            hco_hotel_id: "29",
            hco_category_id: "1",
            hco_id: "26",
            hco_latitude: "21.008668",
            hco_longitude: "105.782587",
            hco_name: "JW Marriott Hotel Hanoi",
            hco_line: "No 8 Do Duc Duc Road",
            hco_rating: "5",
            hco_json_amenities: [24, 4454, 2074, 2168],
            hco_statistics: {
                52: {id: "52", name: "Total number of rooms - 358", value: "358"},
                54: {id: "54", name: "Number of floors - 10", value: "10"},
            },
            discount: "20%",
            image: 'https://images.unsplash.com/photo-1615266895738-11f1371cd7e5?ixid=MXwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyMXx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'
        },
        {
            hco_hotel_id: "49",
            hco_category_id: "1",
            hco_id: "40",
            hco_latitude: "21.034502",
            hco_longitude: "105.852124",
            hco_name: "Essence Hanoi Hotel & Spa",
            hco_line: "22 Ta Hien Street",
            hco_rating: "5",
            hco_json_amenities: [24, 4454, 2074, 2168],
            hco_statistics: {
                52: {id: "52", name: "Total number of rooms - 358", value: "358"},
                54: {id: "54", name: "Number of floors - 10", value: "10"},
            },
            discount: "20%",
            image: 'https://images.unsplash.com/photo-1615257390825-700517a1585a?ixid=MXwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwyMHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60'
        },
    ],
    prices: {
        15: "2119887.000",
        49: "3421842.000",
        29: "2982398.000"
    }
}


initMapSmall();

$('#modal-hotel-map').on('shown.bs.modal', e => {
    initMapList();
});


function initMapSmall() {
    if (mapSmall && google) {
        const mapObj = JSON.parse($(mapSmall).attr('data-map'));
        const location = {
            lat: parseFloat(mapObj.location.lat),
            lng: parseFloat(mapObj.location.lng)
        };
        const icon = {
            url: "/assets/images/pin-2.svg",
            scaledSize: new google.maps.Size(48, 48),
            // labelOrigin: new google.maps.Point(0, -12)
        };
        const map = new google.maps.Map(mapSmall, {
            center: location,
            zoom: parseFloat(mapObj.zoom),
            disableDefaultUI: true
        });
        let marker = new google.maps.Marker({
            position: location,
            map,
            // title: mapObj.name,
            // label: {
            //     className: '',
            //     text: mapObj.name,
            //     fontFamily: 'Nunito',
            //     color: '#000000',
            //     fontWeight: 'bold',
            //     fontSize: '14px'
            // },
            icon: icon,
        });
    }
}

function initMapList() {
    var markers = new Array;
    var bounds = new google.maps.LatLngBounds(); // (1)

    const iconActive = {
        url: "/assets/images/pin-2.svg",
        scaledSize: new google.maps.Size(32, 32),
    };
    const iconDefault = {
        url: "/assets/images/pin-3.svg",
        scaledSize: new google.maps.Size(32, 32),
    };

    // Display result
    
    if (totalArea) $(totalArea).html(data.count);
    displayListing(data.hotels);

    if (mapList && google && data) {
        var _center = {
            lat: parseFloat(data.hotels[0].hco_latitude), 
            lng: parseFloat(data.hotels[0].hco_longitude)
        };
        var map = new google.maps.Map(mapList, {
            zoom: data.zoom,
            maxZoom: 25,
            center: _center
        });
        var infoWindow = new google.maps.InfoWindow();

        for (let i = 0; i < data.hotels.length; i++) {
            // Create marker
            const item = data.hotels[i];
            const _location = {
                lat: parseFloat(item.hco_latitude),
                lng: parseFloat(item.hco_longitude)
            };
            var marker = new google.maps.Marker({
                position: _location,
                map,
                icon: iconDefault,
                zIndex: 1
            });

            // Create tooltip for marker
            var stars = '';
            var _rating = parseFloat(item.hco_rating);
            const starCount = Math.round(_rating);
            for (let i = 0; i < starCount; i++) {
                stars += '<span class="ic-star"></span>';
            }
            const contentString = `
                <div class="card-tooltip">
                    <div class="card-left">
                        <div class="card-bgd bg-reset" style="background-image: url(${item.image})"></div>
                    </div>
                    <div class="card-right">
                        <div class="card-title">${item.hco_name}</div>
                        <div class="rating-bright-star">${stars}</div>
                    </div>
                </div>
            `;
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function () {
                    // Reset for other markers
                    markers.forEach(function (elMarker, idMarker) {
                        infoWindow.open(map, elMarker);
                        elMarker.setIcon(iconDefault);
                        elMarker.setZIndex(1);
                        elMarker.setAnimation(null);
                    });
                    $(listArea).find('.card-hotel').removeClass('active');

                    infoWindow.setContent(contentString);
                    infoWindow.open(map, marker);
                    marker.setIcon(iconActive);
                    marker.setZIndex(2);
                    toggleBounce(marker);

                    const card = document.querySelector(`#hotel-${item.hco_hotel_id}`);
                    const listResultArea = $(listArea).find('.list-item-map')[0];
                    const scrollTop = card.offsetTop;
                    $(card).addClass('active');
                    $(listResultArea).scrollTop(scrollTop - 32);
                }
            })(marker, i));
            google.maps.event.addListener(infoWindow,'closeclick',function(){
                markers[i].setIcon(iconDefault);
                markers[i].setZIndex(1);
                markers[i].setAnimation(null);
            });
            google.maps.event.addListener(map, 'click', function () {
                infoWindow.close();
                markers[i].setIcon(iconDefault);
                markers[i].setZIndex(1);
                markers[i].setAnimation(null);
            });

            document.querySelector(`#hotel-${item.hco_hotel_id}`).addEventListener('click', function (e) {
                $(listArea).find('.card-hotel').removeClass('active');
                markers.forEach(function (elMarker, idMarker) {
                    infoWindow.open(map, elMarker);
                    elMarker.setIcon(iconDefault);
                    elMarker.setZIndex(1);
                    elMarker.setAnimation(null);
                });
                
                const card = $(e.target).hasClass('.card-hotel') ? $(e.target) : $(e.target).closest('.card-hotel');
                $(card).addClass('active');
                google.maps.event.trigger(markers[i], 'click');
            });

            bounds.extend(marker.getPosition()); //(2)
            markers.push(marker);
        }

        map.fitBounds(bounds); //And (1), (2) => show map to cover all markers

        // Overwrite zoom map option after fitbounds
        var listener = google.maps.event.addListener(map, 'idle', function () {
            map.setZoom(13);
            google.maps.event.removeListener(listener);
        });
    }
}

function displayListing(list) {
    if (listArea.length > 0) {
        const listResultArea = $(listArea).find('.list-item-map')[0];
        const listArr        = new Array;

        // Reset list
        $(listResultArea).html('');

        for (let i = 0; i < list.length; i++) {
            const item         = list[i];
            var itemHtml, contentHtml;
            var stars          = '';
            var _rating = parseFloat(item.hco_rating);
            const isAvailable  = item.hco_statistics && !$.isEmptyObject(item.hco_statistics);
            const price        = formatedPrice(findPrice(item.hco_hotel_id));
            const priceSale    = item.discount ? formatedPrice(findPrice(item.hco_hotel_id) * ( 100 - parseFloat(item.discount)) / 100) : '';
            console.log(parseFloat(item.discount));
            const starCount    = Math.round(_rating);
            const classSpacing = i == list.length - 1 ? '' : 'mb-3';
            

            for (let j = 0; j < starCount; j++) {
                stars += '<span class="ic-star"></span>';
            }

            if (isAvailable) {
                contentHtml = `
                    <div class="card-content__right">
                        <div class="card-price">
                            <span class="card-price-num card-price-num--old">${price}</span>
                            <span class="card-price-new">${priceSale}</span>
                        </div>
                        <div class="text-sm-italic">5 người/đêm</div>
                        <a class="button button-primary button--small" href="#">Chọn phòng này</a>
                    </div>
                `;
            } else {
                contentHtml = `
                    <div class="card-content__right">
                        <div class="text-red text-strong mt-4">Hết phòng</div>
                        <div class="text-sm-italic">Bạn vừa bỏ lỡ mất phòng này</div>
                    </div>
                `;
            }

            itemHtml = `
                <div class="card-hotel card-hotel--shortcut ${classSpacing}" id="hotel-${item.hco_hotel_id}">
                    <header class="card-header card-header--small">
                        <div class="card-left">
                            <div class="card-image">
                                <div class="card-bgd bg-reset" style="background-image: url(${item.image})"></div>
                            </div>
                            <div class="card-content">
                                <a href="#" class="card-title">
                                    ${item.hco_name}
                                    <div class="rating-bright-star">${stars}</div>
                                </a>
                                <div class="evaluate-wrapper">
                                    <div class="rating-point">
                                        <span>${_rating}/</span>
                                        <span>5</span>
                                    </div>
                                    <div class="card-label-left bgd-red">
                                        Giảm ${item.discount}
                                        <span></span>
                                    </div>
                                </div>
                                ${contentHtml}
                            </div>
                        </div>
                    </header>
                </div>
            `;

            $(listResultArea).append(itemHtml);
        }
    }
}

function toggleBounce(marker) {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}

function scrollToElement(parent, element) {
    let scrollTop = $(element).offset().top;
    $(parent).scrollTop(scrollTop);
}

function findPrice(id) {
    if (data) {
        return Math.floor(data.prices[id])
    } else {
        return null
    }
}
