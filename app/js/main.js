// Common
Array.prototype.remove = function() {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

// Window ready
inputActive();

window.onload = () => {
  dropdown();
  slider();
  modal();
  handleModalGallery();
  handleModalGalleryRoom();
  handleModalRoomDetail();
  chip();
  radioTogglePickDate();
  pickDate(isSingleDate || false);
  pickSingleDate();
  pickDateRelative(); // for both input from & to date
  buttonReverse();
  dropdownInput();
  inputNumber();
  inputNumber2();
  listingPick();
  dropdownCustomerSelect();
  closeButton();
  filterGrid();
  // validateForm();
  showCardFlightDetail();
  toggleResultColumn();
  toggleElement();
  filterTag();
  handleCollapse();
  handleCopy();
  handleToggleMultipleElements();
  basicScrollTop();
  handleAutocompleteAccordion();
  rating();
  handleTriggerInputGroup();
  handleCardTotalHotelDetail();
  toggleHotelRooms();
  // handleHiddenWhenSticky();
  handleToggleCheckbox();
  handleTextMore();
  handleSliderAbout();
  handleInputCountChildren();
  pickMonthYear();

  $('[data-toggle="tooltip"]').tooltip();
};

$( window ).resize(function() {
  handleModalGallery();
  handleModalGalleryRoom();
  handleModalRoomDetail();
});

$('[data-toggle="tooltip"]').tooltip();

$.dateRangePickerLanguages['custom'] =
	{
		'selected': 'Chọn:',
		'days': 'Ngày',
		'apply': 'Đóng',
		'week-1' : 'T2',
		'week-2' : 'T3',
		'week-3' : 'T4',
		'week-4' : 'T5',
		'week-5' : 'T6',
		'week-6' : 'T7',
		'week-7' : 'CN',
		'month-name': ['Tháng 1','Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6','Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
		'shortcuts' : 'Chọn nhanh',
		'past': 'Trước',
		'7days' : '7days',
		'14days' : '14days',
		'30days' : '30days',
		'previous' : 'Previous',
		'prev-week' : 'Tuần',
		'prev-month' : 'Tháng',
		'prev-quarter' : 'Quý',
		'prev-year' : 'Năm',
		'less-than' : 'Khoảng thời gian cần lớn hơn %d ngày',
		'more-than' : 'Khoảng thời gian cần nhỏ hơn %d ngày',
		'default-more' : 'Vui lòng chọn khoảng thời gian lớn hơn %d ngày',
		'default-less' : 'Vui lòng chọn khoảng thời gian nhỏ hơn %d ngày',
		'default-range' : 'Vui lòng chọn khoảng thời gian từ %d đến %d ngày',
		'default-default': 'Ngôn ngữ Tiếng Việt'
	};

function formatedPrice(price) {
  var formatter = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  });
  return formatter.format(price).replace(/₫/, 'VND');
}

function dropdown(callback) {
  let $dropdown = $('.dropdown');
  let classActive = 'is-showing';
  if ($dropdown.length > 0) {
    $dropdown.each((index, dropdown) => {
      let button = $(dropdown).find('.dropdown__button')[0];

      $(button).on('click', e => {
        // if(e.target.dataset.name === 'deleteButton') {
        //   e.preventDefault();
        //   e.stopPropagation();
        // }
        e.preventDefault();

        let $parent = $(e.target).closest('.dropdown');
        $parent.hasClass(classActive) ? animateHide($parent) : animateShow($parent);

        // Close other dropdown
        $('body .dropdown.is-showing').each((index, dropdown) => {
          if (!$(dropdown).is($parent)) animateHide(dropdown);
        });
      });
    });

    // Close dropdown when click outside
    $(document).on('click', e => {
      let $dropdownParent = $(e.target).closest('.dropdown');

      if ($dropdownParent.length == 0) {
        $('body .dropdown').each((index, dropdown) => {
          if ( !$(dropdown).is(e.target) && $dropdown.has(e.target).length == 0 ) animateHide($(dropdown));
        });
      }

      return;
    });

    function animateShow(dropdown) {
      $(dropdown).addClass(classActive);
    }

    function animateHide(dropdown) {
      if ($(dropdown).hasClass(classActive)) {
        $(dropdown).removeClass(classActive).addClass('hidding');
        setTimeout(() => {
          $(dropdown).removeClass('hidding');
        }, 500);
      }
    }

    $('.dropdown .dropdown__item').click(e => {
      let self = e.currentTarget;
      if ($(self).prop('tagName') !== 'A') {
        e.preventDefault();
        let menu = $(self).closest('.dropdown__menu');
        $(menu).find('.dropdown__item').removeClass('active');
        setTimeout(() => {
          $(self).addClass('active');
        }, 100);
      } else {
        let dropdown = $(self).closest('.dropdown');
        animateHide(dropdown);
        if (typeof callback == "function") callback();
      }
    });

    $('.dropdown .button-close').click(e => {
      e.preventDefault();
      const dropdown = e.currentTarget.closest('.dropdown');
      animateHide($(dropdown));
      if (typeof callback == "function") callback();
    });
  }

  let $dropdownReplace = $('.js-dropdown-replace');
  if ($dropdownReplace.length > 0) {
    let item = $('.js-dropdown-replace .dropdown__item');
    $(item).click(e => {
      e.preventDefault();
      const _self             = e.currentTarget;
      const dropdown          = $(_self).closest('.js-dropdown-replace');
      const itemCurrentActive = $(dropdown).find('.dropdown__item.active')[0];
      const dropdownButton    = $(dropdown).find('.dropdown__button')[0];

      if (itemCurrentActive) $(itemCurrentActive).removeClass('active');
      $(_self).addClass('active');

      const str = dropdownButton.innerHTML;
      const newStr = str.replace(dropdownButton.innerText, _self.innerText);
      dropdownButton.innerHTML = newStr;

      return false;
    });
  }

  let $dropdownReplaceMultiple = $('.js-dropdown-replace-multiple');
  if ($dropdownReplaceMultiple.length > 0) {
    let menus = $('.js-dropdown-replace-multiple .dropdown__menu');
    $(menus).each((index, menu) => {
      let item = $(menu).find('.dropdown__item');
      $(item).click(e => {
        e.preventDefault();
        const _self             = e.currentTarget;
        const dropdown          = $(_self).closest('.js-dropdown-replace-multiple');
        const itemCurrentActive = $(dropdown).find('.dropdown__item.active')[0];
        const target            = $(_self).closest('.dropdown__menu').attr('data-target');
        const targetEl          = $(dropdown).find(`[data-replace="${target}"]`)[0];
        
        if (targetEl) {
          const text = $(_self).text();
          $(targetEl).text(text);
        }
      });
    });
  }
}

function slider() {
  let $slider = $('.slider-hero');
  if ($slider.length > 0) {
    var options = {
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      fade: true,
      cssEase: 'ease-in-out',
      autoplay: true,
      autoplaySpeed: 5000,
      prevArrow: '<button type="button" class="slider__control slider__control--left slider__control--large"><span class="ic-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slider__control slider__control--right slider__control--large"><span class="ic-chevron-right"></span></button>'
    };
    $slider.slick(options);
  }

  let $slider3 = $('.slider-3');
  if ($slider3.length > 0) {
    var options = {
      slidesToShow: 3,
      slidesToScroll: 1,
      speed: 500,
      lazyLoad: 'progressive',
      cssEase: 'ease-in-out',
      prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    };
    $slider3.slick(options);
  }

  let $slider4 = $('.slider-4');
  if ($slider4.length > 0) {
    var options = {
      slidesToShow: 4,
      slidesToScroll: 1,
      speed: 500,
      lazyLoad: 'progressive',
      cssEase: 'ease-in-out',
      prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    };
    $slider4.slick(options);
  }

  let $sliderAuto = $('.slider-auto');
  if ($sliderAuto.length > 0) {
    var options = {
      variableWidth: true,
      infinite: false,
      prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
      nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>'
    };
    $sliderAuto.slick(options);
  }

  // $('.modal-gallery').on('shown.bs.modal', (e) => {
  //   let $sliderGallery = $('.modal-gallery .slider-gallery');
  //   let $sliderGalleryNav = $('.modal-gallery .slider-gallery-nav');

  //   if ($sliderGallery.length > 0) {
  //     var options = {
  //       slidesToShow: 1,
  //       slidesToScroll: 1,
  //       arrows: true,
  //       fade: false,
  //       adaptiveHeight: true,
  //       infinite: false,
  //       useTransform: true,
  //       dots: true,
  //       // speed: 200,
  //       cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
  //       prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
  //       nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>'
  //     };

  //     // Show slide number
  //     $sliderGallery.on('init', function (event, slick, currentSlide, nextSlide) {
  //       // no dots -> no slides
  //       if(!slick.$dots){
  //         return;
  //       }

  //       // use dots to get some count information
  //       $sliderGallery.find('.caption-number').text(1 + '/' + (slick.$dots[0].children.length));
  //     });

  //     $sliderGallery.not('.slick-initialized').slick(options);

  //     $sliderGalleryNav.on('init', function(event, slick) {
  //       $('.slider-gallery-nav .slick-slide.slick-current').addClass('is-active');
  //     })
  //     .not('.slick-initialized')
  //     .slick({
  //       slidesToShow: 5.4,
  //       slidesToScroll: 5.4,
  //       dots: false,
  //       focusOnSelect: false,
  //       infinite: false,
  //       arrows: false,
  //       centerPadding: '5px',
  //     });

  //     $sliderGallery.on('afterChange', function(event, slick, currentSlide) {
  //       //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
  //       var i = (currentSlide ? currentSlide : 0) + 1;
  //       // use dots to get some count information
  //       $sliderGallery.find('.caption-number').text(i + '/' + (slick.$dots[0].children.length));

  //       $sliderGalleryNav.slick('slickGoTo', currentSlide);
  //       var currrentNavSlideElem = '.slider-gallery-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
  //       $('.slider-gallery-nav .slick-slide.is-active').removeClass('is-active');
  //       $(currrentNavSlideElem).addClass('is-active');
  //     });

  //     $sliderGalleryNav.on('click', '.slick-slide', function(event) {
  //       event.preventDefault();
  //       var goToSingleSlide = $(this).data('slick-index');

  //       $sliderGallery.slick('slickGoTo', goToSingleSlide);
  //     });
  //   }
  // });
}

function handleModalGalleryRoom() {
  const hotelDetailGallery = $('.hotel-detail__gallery');
  if (hotelDetailGallery.length) {
    const sliderGalleryWrapper = $('.modal-gallery-room .slider-gallery');
    const sliderGalleryNavWrapper = $('.modal-gallery-room .slider-gallery-nav');
    $('.hotel-detail__gallery div.img-gallery').on('click', function (e) {
      let element = $(this);
      e.preventDefault();

      // Call API for images data modal hotel rooms
      if (sliderGalleryWrapper.is(':empty')) {
        $.ajax({
          url: sliderGalleryWrapper.data('json'),
          dataType: 'JSON',
          method: 'GET'
        }).done(function(data) {
          data.forEach(img => {
            const galleryImg = `
              <div class="gallery-img bg-reset" style="background-image: url('${img.url_full}');">
                <div class="caption">
                  <div class="caption-title">${img.caption}</div>
                  <div class="caption-number"></div>
                </div>
              </div>
            `;
            sliderGalleryWrapper.append(galleryImg);

            const galleryImgNav = `<div class="gallery-img bg-reset" style="background-image: url('${img.url_thumb}');"></div>`;
            sliderGalleryNavWrapper.append(galleryImgNav);
          });

          setTimeout(() => {
            $('.modal-gallery-room').modal('show');
          }, 1000);
        });
      } else {
        $('.modal-gallery-room').modal('show');
      }
      $('.modal-gallery-room').one('shown.bs.modal', (e) => {
        let gotoSlide = element.data('goto-slide');
        let $sliderGallery = $('.modal-gallery-room .slider-gallery');
        let $sliderGalleryNav = $('.modal-gallery-room .slider-gallery-nav');
        if ($sliderGallery.length > 0) {
          if ($sliderGallery.hasClass('slick-initialized')) {
            $sliderGallery.slick('slickGoTo', gotoSlide);
          } else {
            let options = {
              lazyLoad: 'ondemand',
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: true,
              fade: false,
              adaptiveHeight: true,
              infinite: false,
              useTransform: true,
              dots: true,
              initialSlide: gotoSlide,
              lazyLoad: 'ondemand',
              cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
              prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
              nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>'
            };

            // Show slide number
            // Init slider
            $sliderGallery.on('init', function (event, slick, currentSlide) {
              // no dots -> no slides
              if(!slick.$dots){
                return;
              }

              // use dots to get some count information
              $sliderGallery.find('.caption-number').text((gotoSlide + 1) + '/' + (slick.$dots[0].children.length));
            });

            $sliderGallery.slick(options);

            $sliderGalleryNav.on('init', function(event, slick) {
              $(`.modal-gallery-room .slider-gallery-room-nav .slick-slide[data-slick-index="${gotoSlide}"]`).addClass('is-active');
            }).slick({
              slidesToShow: 8.5,
              slidesToScroll: 8.5,
              dots: false,
              focusOnSelect: false,
              infinite: false,
              arrows: false,
              centerPadding: '5px',
              initialSlide: gotoSlide,
              lazyLoad: 'ondemand',
            });

            $sliderGallery.on('afterChange', function (event, slick, currentSlide, nextSlide) {
              // no dots -> no slides
              if(!slick.$dots){
                return;
              }

              let i = (currentSlide ? currentSlide : 0) + 1;
              // Show slide number
              $sliderGallery.find('.caption-number').text(i + '/' + (slick.$dots[0].children.length));

              // Handle event on sliderGalleryNav
              $sliderGalleryNav.slick('slickGoTo', currentSlide);
              let currrentNavSlideElem = '.modal-gallery-room .slider-gallery-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
              $('.modal-gallery-room .slider-gallery-nav .slick-slide.is-active').removeClass('is-active');
              $(currrentNavSlideElem).addClass('is-active');
            });

            $sliderGalleryNav.on('click', '.slick-slide', function(e) {
              e.preventDefault();
              var goToSingleSlide = $(this).data('slick-index');

              $sliderGallery.slick('slickGoTo', goToSingleSlide);
            });
          }
        }
      });
    });
  }
}

function radioTogglePickDate() {
  let $radio = $('.radio .radio__control');

  if ($radio.length > 0) {
    checkRadioTarget($radio);

    $radio.on('change', (e) => {
      let target = $(e.target).data('target');

      if (target) {
        $(target).toggle();
        $(target).toggleClass('is-show');
        const datePicker = $(target).closest('.form-group');
        if (datePicker) {
          $(datePicker).data('dateRangePicker').destroy();
          setTimeout(() => pickDate(!$(target).hasClass('is-show')), 100);
        }
        return false;
      }
    });
  }

  function checkRadioTarget(radio) {
    let target = $(radio).data('target');
    if (target) {
      let isShow = $(target).data('show');
      isShow ? $(target).show() : $(target).hide();
    }
  }
}

function inputActive() {
  let $inputGroup = $('.input-group');
  if ($inputGroup.length > 0) {
    $inputGroup.on('click', e => {
      $inputGroup.removeClass('active');
      let $group = $(e.currentTarget);
      let $target = $(e.target);
      let targetNodeName = $target.prop('tagName');
      if (targetNodeName == 'INPUT' || targetNodeName == 'SELECT') {
        $group.addClass('active');
      }
    });
    $(document).on('click', e => {
      if (e.target.closest('.input-group')) return;
      $inputGroup.removeClass('active');
    });
  }
}

function pickDate(isSingleDate) {
  let formGroup = $('.js-form-datepicker'); // .form-group
  if (formGroup.length > 0) {
    formGroup.each((index, group) => {
      const dateFrom       = $(group).find('.input-group-from-date input')[0];
      const dateTo         = $(group).find('.input-group-to-date input')[0];
      const $dateFrom      = $(dateFrom);
      const $dateTo        = $(dateTo);
      const $parent        = $dateFrom.closest('.form-group');
      const countDateArea = $(group).find('.total-date')[0] || null;

      let t                = new Date();
      let today            = t.getDate() + '/' + (t.getMonth() + 1) + '/' + t.getFullYear();
      let nextThreeDays    = (t.getDate() + 3) + '/' + (t.getMonth() + 1) + '/' + t.getFullYear();
      let dateArr          = [];
      let monthArr         = [];
      let timeLabel        = window.timeLabel;
      let lang             = window.lang;
      const formatDate     = 'DD/MM/YYYY';

      switch (lang) {
        case 'vi':
          _setLangLabel('vi');
          break;
        default: // Default lang = en
          _setLangLabel('en');
      };

      if ($dateFrom.length > 0) {
        /*
          jQuery Daterangepicker: https://longbill.github.io/jquery-date-range-picker/
          Events:
          - datepicker-first-date-selected: e, obj
          - datepicker-change: e, obj
          - datepicker-apply: e, obj
          - datepicker-close
          - datepicker-closed
          - datepicker-open
          - datepicker-opened
        */
        let config = {
          autoClose: true,
          singleDate: isSingleDate,
          format: formatDate,
          separator: ' to ',
          language: 'custom',
          // startDate: $dateFrom.val(),
          startDate: moment().format(formatDate),
          selectForward: true,
          stickyMonths: true,
          getValue: () => {
            if (!isSingleDate) {
              // Set date to
              const newStartDate   = moment($dateFrom.val(), formatDate);
              const currentEndDate = moment($dateTo.val(), formatDate);
              let newEndDate;

              if (newStartDate.isAfter(currentEndDate)) {
                newEndDate = $dateFrom.val();
                $dateTo.val(newEndDate);
              } else {
                newEndDate = $dateTo.val();
              }

              return $dateFrom.val() + ' to ' + newEndDate;
            } else {
              return $dateFrom.val();
            }
          },
          setValue: (s, s1, s2) => {
            $dateFrom.val(s1);
            if (!isSingleDate) $dateTo.val(s2);
          }
        };
        $parent.dateRangePicker(config)
        .bind('datepicker-closed', () => {
          $parent.find('.input-group').each((index, input) => $(input).removeClass('active') );
        })
        .bind('datepicker-change', (e, obj) => {
          /* This event will be triggered when second date is selected */
          const value1 = obj.value.split(' to ')[0];
          const newStartDate   = moment(value1, formatDate);
          const currentEndDate = moment($dateTo.val(), formatDate);

          if (newStartDate.isAfter(currentEndDate)) {
            $dateTo.val(value1);
          }

          if (countDateArea) {
            const countDate = currentEndDate.diff(newStartDate, 'days') + 1;
            const textDay   = $(countDateArea).attr('data-text-day') || 'ngày';
            const textNight = $(countDateArea).attr('data-text-night') || 'đêm';

            if (countDate > 1) {
              countDateArea.innerText = `${countDate} ${textDay}, ${(countDate - 1)} ${textNight}`;
            } else {
              countDateArea.innerText = `${countDate} ${textDay}`;
            }
          }
        });
      }

      function _setLangLabel(language = 'en') {
        for (let key in timeLabel.dates) {
          dateArr.push(timeLabel.dates[key][language + '-short']);
        }
        for (let key in timeLabel.months) {
          monthArr.push(timeLabel.months[key][language]);
        }
      }
    });
  }
}

function pickSingleDate() {
  let $el         = $('.js-pick-single-date input');
  let $icon       = $('.js-pick-single-date .input-group-icon');
  const lang      = window.lang;
  const timeLabel = window.timeLabel;
  const config = {
    format: 'DD/MM/YYYY',
    autoClose: true,
    language: 'custom',
    singleDate : true,
    singleMonth: true,
    endDate: moment().endOf('day').format('DD/MM/YYYY'),
    monthSelect: true,
    yearSelect: [1920, moment().get('year')]
  };

  if ($el.length > 0) {
    $el.each((index, input) => {
      $(input).dateRangePicker(config).bind('datepicker-change', (e, obj) => {
        $(input).trigger('change');
      });
    });
  }

  if ($icon.length > 0) {
    $icon.click(e => {
      e.stopPropagation();
      const self = $(e.currentTarget);
      const input = self.closest('.js-pick-single-date').find('input')[0];
      $(input).data('dateRangePicker').open();
    });
  }
}

function pickDateRelative() {
  let $formGroup = $('.js-pick-date-relative');
  $formGroup.css('position', 'relative');

  if ($formGroup.length > 0) {
    $formGroup.each((index, group) => {
      const dateFrom   = $(group).find('.input-group-from-date input')[0];
      const dateTo     = $(group).find('.input-group-to-date input')[0];
      const $dateFrom  = $(dateFrom);
      const $dateTo    = $(dateTo);
      const $parent    = $dateFrom.closest('.form-group');
      const formatDate = 'DD/MM/YYYY';
      const today      = moment(new Date()).format(formatDate);

      if ($dateFrom.length > 0) {
        let config = {
          autoClose: true,
          singleDate: true,
          format: formatDate,
          language: 'custom',
          startDate: today,
          selectForward: true,
          stickyMonths: true,
          container: $parent,
          setValue: (s) => {
            $dateFrom.val(s);
          }
        }
        $dateFrom.dateRangePicker(config)
        .bind('datepicker-change', (e, obj) => {
          if ($dateTo.length > 0) {
            const newStartDate = moment(obj.value, formatDate);
            const currentEndDate = moment($dateTo.val(), formatDate);
            let newEndDate;

            if (newStartDate.isAfter(currentEndDate)) {
              newEndDate = $dateFrom.val();
              $dateTo.val(newEndDate);
            }
          }
        })
        .bind('datepicker-closed', (e, obj) => {
          if ($dateTo.is(':visible')) {
            $dateTo.data('dateRangePicker').open();
          }
        });

        if ($dateTo.length > 0) {
          let configTo = {
            autoClose: true,
            singleDate: false,
            format: formatDate,
            separator: ' to ',
            language: 'custom',
            selectForward: true,
            stickyMonths: true,
            // getValue: () => {
            //   return _checkNewEndDate();
            // },
            setValue: (s, s1, s2) => {
              $dateTo.val(s2)
            }
          }
          $dateTo.dateRangePicker(configTo)
          .bind('datepicker-opened', () => {
            const startDate = $dateFrom.val();
            $dateTo.data('dateRangePicker').setStart(startDate);
          });

          function _checkNewEndDate() {
            let newStartDate   = moment($dateFrom.val(), formatDate);
            let currentEndDate = moment($dateTo.val(), formatDate);
            let newEndDate;

            if (newStartDate.isAfter(currentEndDate)) {
              newEndDate = $dateFrom.val();
              $dateTo.val(newEndDate);
            } else {
              newEndDate = $dateTo.val();
            }

            return newEndDate
          }
        }
      }
    });
  }
}

function buttonReverse() {
  let $button = $('.button-icon-reverse');
  if ($button.length > 0) {
    $button.on('click', e => {
      e.preventDefault();

      // Animate
      $(e.currentTarget).addClass('is-animating');
      setTimeout(() => {
        $(e.currentTarget).removeClass('is-animating');
      }, 500);

      // Reverse value
      let target1 = $(e.currentTarget).data('target-1');
      let target2 = $(e.currentTarget).data('target-2');

      if ($(target1) && $(target2)) {
        let t1 = $(target1).val();
        let t2 = $(target2).val();
        $(target1).val(t2);
        $(target2).val(t1);
      }
    });
  }
}

function dropdownInput() {
  let $dropdown = $('.input-group.dropdown');
  if ($dropdown.length > 0) {
    let item = $dropdown.find('.dropdown__body .listing-large-item > a, .dropdown__body .list-media-item .media-icon');
    if (item.length > 0) {
      $(item).click(e => {
        e.preventDefault();
        const id              = $(e.currentTarget).data('id');
        const name            = $(e.currentTarget).data('name');
        const $parent         = $(e.currentTarget).closest('.input-group');
        const input           = $parent.find('.input-group-control')[0];
        const $dropdownParent = $(e.currentTarget).closest('.dropdown');

        if (id && name) {
          $(input).val(name + ' (' + id + ')');
        } else if (name) {
          $(input).val(name);
        }

        setTimeout(() => $dropdownParent.removeClass('is-showing'), 100);

        // $parent.removeClass('active');
        return false;
      });
    }
    $dropdown.on('click', e => {
      let dropdown = $(e.target).closest('.dropdown');
      $(dropdown).addClass('is-showing');
      _calculatePosition($(dropdown));
      return false;
    });

    function _calculatePosition(dropdown) {
      const windowHeight        = $(window).height();
      const dropdownBody        = $(dropdown).find('.dropdown__body');
      const dropdownHeight      = $(dropdownBody).height();
      const dropdownOffsetTop   = $(dropdown).offset().top - $(window).scrollTop();
      const isdropdownOutWindow = dropdownOffsetTop + dropdownHeight > windowHeight;

      if (isdropdownOutWindow) {
        let spacingOff = dropdownOffsetTop + dropdownHeight - windowHeight + 30;
        $(dropdownBody).css({ top: - spacingOff + 'px' });
      } else {
        $(dropdownBody).css({ top: $(dropdown).height() + 10 + 'px' });
      }
    }
  }
}

function inputNumber() {
  let $inputGroup = $('.input-group-number');
  if ($inputGroup.length > 0) {
    $inputGroup.each((index, group) => {
      let input = $(group).find('.input-group-control')[0];
      let $btnMine = $(input).prev();
      let $btnAdd = $(input).next();
      const min = parseInt($(input).data('min')) || 0;
      const max = parseInt($(input).data('max'));

      if ($btnMine && $btnAdd) {
        $btnMine.bind('click', e => {
          let value = $(input).val();
          if (value > min) {
            value--;
            $(input).val(value);
            $(input).trigger('change');
          };
          return false;
        });
        $btnAdd.bind('click', e => {
          let value = $(input).val();
          value++;
          if ((min || max) && !(value >= min && value <= max)) return false;
          $(input).val(value);
          $(input).trigger('change');
          return false;
        });
      }

      $(input).bind('change', e => {
        let formGroup = $(e.target).closest('.input-group');
        let langClient = $(formGroup).attr('data-lang-client');
        let langRoom = $(formGroup).attr('data-lang-room');
        let type = $(e.target).attr('data-type');

        if (formGroup.length > 0) {
          let inputs = $(formGroup).find('input');
          let seatActive = $(formGroup).find('.listing-pick-item.active a');
          let sum = 0; // Sum clients
          let sumRoom = 0;

          let textRoom, textClient, textSeat;

          inputs.each((index, el) => {
            if (index !== 0) {
              const value = parseInt($(el).val());
              const elType = $(el).attr('data-type');

              switch (elType) {
                case 'room':
                  sumRoom += value;
                  break;
                default: // default types of clients: adult, baby, children
                  sum += value;
              };
            }
          });

          if (langRoom) textRoom = `${sumRoom} ${langRoom}` || null;
          if (langClient) textClient = `${sum} ${langClient}` || null;
          if (seatActive) textSeat = $(seatActive).text() || null;

          const finalValue = [textRoom, textClient, textSeat].filter(el => el != null).join(', ');
          $(inputs[0]).val(finalValue);
          $(inputs[0]).attr('data-' + type, $(e.target).val());
          $(inputs[0]).attr('title', finalValue);
        }

        return false;
      });
    });
  }
}

function handleInputCountChildren() {
  const inputGroup = $('.js-input-count-children');
  if (inputGroup.length > 0) {
    $(inputGroup).each((index, group) => {
      const area = $(group).attr('data-select-area');
      const input = $(group).find('input')[0];
      const title = $(group).find('.input-group-title').text().trim();
      const selectHtml = $(area).html();

      $(input).bind('change', e => {
        const count = parseInt(e.target.value);
        const countSelect = $(area).find('.input-group-select').length;

        if (count > 0) {
          $(area).show();

          if (count > 1 && count > countSelect) {
            _addSelect()
          } else if (count >= 1 && count < countSelect) {
            _removeSelect();
          }
        } else {
          $(area).hide();
          $(area).html(selectHtml);
        }
        return false;

        function _addSelect() {
          let selectHtmlClone = $.parseHTML(selectHtml);
          const subtitle = title + ' ' + (count) + ':';
          const name = 'children-age-' + count;
          $(selectHtmlClone[1]).find('.input-group-label').text(subtitle);
          $(selectHtmlClone[1]).attr('name', name);
          $(area).append(selectHtmlClone);
        }

        function _removeSelect() {
          $(area).find('.input-group-select')[countSelect - 1].remove();
        }
      });
    });
  }
}

function inputNumber2() {
  let inputGroup = $('.input-group-number-2');
  if (inputGroup.length > 0) {
    inputGroup.each((index, group) => {
      let input = $(group).find('.input-group-control');
      let $btnMine = $(group).find('.ic-chevron-down');
      let $btnAdd = $(group).find('.ic-chevron-up');
      const min = parseInt($(input).data('min'));
      const max = parseInt($(input).data('max'));

      if ($btnMine && $btnAdd) {
        $btnMine.bind('click', e => {
          let value = $(input).val();
          let requirement = $(input).data('min') || 0;
          if (value > requirement) {
            value--;
            if (0 < value && value.toString().length < 2) {
              value = "0" + value;
            }
            $(input).val(value);
          };
        });
        $btnAdd.bind('click', e => {
          let value = $(input).val();
          value++;
          if ((min || max) && !(value >= min && value <= max)) return false;
          if (0 < value && value.toString().length < 2) {
            value = "0" + value;
          }
          $(input).val(value);
        });
      }
    });
  }
}

function listingPick() {
  let $item = $('.listing-pick-item');
  if ($item.length > 0) {
    $item.on('click', e => {
      e.preventDefault();
      let items = $(e.target).closest('.listing-pick').find('.listing-pick-item');
      items.each((index, item) => $(item).removeClass('active'));
      $(e.currentTarget).addClass('active');
    });
  }
}

function dropdownCustomerSelect() {
  let $button = $('.dropdown-customer-select .button-submit');
  $button.on('click', e => {
    e.preventDefault();
    e.stopPropagation();

    let $dropdown = $(e.target).closest('.dropdown-customer-select');
    changeValue($dropdown);
  });

  let $item = $('.dropdown-customer-select .listing-pick-item');
  if ($item.length > 0) {
    $item.on('click', e => {
      let $dropdown = $(e.target).closest('.dropdown-customer-select');
      changeValue($dropdown);
    });
  }

  function changeValue(dropdown) {
    let $dropdown     = $(dropdown);
    let input         = $dropdown.find('.input-group-control')[0];
    let adult         = $dropdown.find('[name = "count-adult"]')[0];
    let children      = $dropdown.find('[name = "count-children"]')[0];
    let baby          = $dropdown.find('[name = "count-baby"]')[0];
    let room          = $dropdown.find('[name = "count-room"]')[0];
    let seatClass     = $($dropdown.find('.listing-pick-item.active a')[0]).text() || null;

    let totalAdult    = adult ? parseInt($(adult).val()) : null;
    let totalChildren = children ? parseInt($(children).val()) : null;
    let totalBaby     = baby ? parseInt($(baby).val()) : null;

    let sumClient     = function sum() {
      let total;
      let arr = [totalAdult, totalChildren, totalBaby].filter(el => el != null);
      if (arr.length > 0) total = arr.reduce((x, y) => x + y, 0);
      return total;
    };
    let seatText      = $button.closest('.input-group').attr('data-lang-client') || 'Hành khách';
    let roomText      = $button.closest('.input-group').attr('data-lang-room') || 'Phòng';

    roomText = room ? [room.value, roomText].join(' ') : null;
    seatText = sumClient() != null ? [sumClient(), seatText].join(' ') : null;

    const customerCount = [roomText, seatText, seatClass].filter(el => el != null).join(', ');

    $(input).val(customerCount);
    $(input).attr('title', customerCount);
    if (adult) $(input).attr('data-adult', $(adult).val());
    if (children) $(input).attr('data-children', $(children).val());
    if (baby) $(input).attr('data-baby', $(baby).val());
    if (room) $(input).attr('data-room', $(room).val());
    $dropdown.removeClass('is-showing');
  }
}

function closeButton() {
  let $button = $('.button-close');
  if ($button.length > 0) {
    $button.click(e => {
      e.preventDefault();
      let target = $(e.currentTarget).data('toggle-target');
      let $target = $(e.currentTarget).closest(target);

      if ($target.length > 0) {
        $target.hide();
      }
      return false;
    });
  }
}

function filterGrid() {
  // let $filterGroup = $('.filter-group:not(".filter-group-4-columns")');
  let $filterGroup = $('.js-filter-group');

  if ($filterGroup.length > 0) {
    $filterGroup.each((index, group) => {
      let filter        = $(group).find('.filter')[0];
      let filterButtons = $(group).find('.filter-button-group')[0];
      let valueDefault  = $(filter).attr('data-default');
      let iso = $(filter).isotope({
        itemSelector: '.filter-item',
        filter: valueDefault,
        layoutMode: 'fitRows'
      });

      $(filterButtons).on('click', 'button', e => {
        e.preventDefault();
        let filterValue = $(e.currentTarget).attr('data-filter');
        $(filter).isotope({ filter: filterValue });
        $(filterButtons).find('.active').removeClass('active');
        $(filterButtons).find('[data-filter="' + filterValue + '"').addClass('active');
      });


      iso.on('layoutComplete', (e) => {
        let $filter = $(e.target);
        // Reset slider 3
        let sliders = $filter.find('.slider-3');
        if (sliders.length > 0) {
          sliders.each((index, slider) => {
            $(slider).get(0).slick.setPosition();
            let height = slider.offsetHeight;
            if (height > 0) {
              $filter.height(height);
            }
          });
        }

        return false;
      });

      iso.on('removeComplete', (e) => {
        if ($filter.closest('.filter-group').hasClass('filter-group-4-columns')) {
          let items = $filter.find('.filter-item');
          items.each((index, item) => {
            // $(item).removeClass('reset-position');
          });
        }
      });
    });
  }

  let $mulFilterGroup = $('.js-multi-filter-group');

  if (($mulFilterGroup).length > 0 ) {
    let button = $('.js-multi-filter-group .button-tag');
    button.on('click', function(){
      $(this).toggleClass('active');
    })
  }
}

function validateForm() {
  if ($('form').length > 0) {
    $('form').validate({
      rules: {
        mobile: {
          required: true,
          rangelength: 10,
          number: true
        }
      }
    })
  }
}

function showCardFlightDetail() {
  let cards = $('.card-flight');
  if (cards.length > 0) {
    let toggleButton = $('.card-flight .button-toggle');
    toggleButton.click(e => {
      e.preventDefault();
      const $button = $(e.currentTarget);
      const card = $button.closest('.card-flight');

      animate(card);
    });

    let cardHeader = $('.card-flight .card-header');
    $(cardHeader).click(e => {
      const _self = e.currentTarget;
      const card = $(_self).closest('.card-flight');
      const btnSelect = $(card).find('.card-header .button')[0];

      if (!$(_self).is(btnSelect)) {
        animate(card);
      }
    });

    function animate(card) {
      // Animate show card body
      if (!$(card).hasClass('is-active')) {
        $(card).addClass('is-active');
      } else {
        $(card).removeClass('is-active');
        $(card).addClass('is-hiding');
        setTimeout(() => {
          $(card).removeClass('is-hiding');
        }, 500);
      }

      // Set height for first time open
      const filterGroup = $(card).find('.filter-group')[0];
      if (filterGroup) {
        const filter             = $(filterGroup).find('.filter')[0];
        const filterButtonActive = $(filterGroup).find('.button-tag.active')[0];
        const indexActive        = $(filterButtonActive).attr('data-filter');
        const paneActive         = $(filter).find(indexActive)[0];

        $(filter).css({
          'height': paneActive.offsetHeight
        });
      }

    }
  }
}

function toggleResultColumn() {
  const $section = $('.search-result');
  if ($section.length > 0) {
    let column       = $section.find('.col-result');
    const colLeave   = $section.find('.col-leave')[0];
    const colArrived = $section.find('.col-arrived')[0];

    $(column).click(e => {
      const columnActive        = $(e.currentTarget);
      const parent              = $(columnActive).closest('.search-result');
      const columnCurrentActive = $(parent).find('.col-result.is-result-expand');

      if (!columnActive.hasClass('is-result-expand') && !columnActive.hasClass('is-selected')) {
        $(columnCurrentActive).removeClass('is-result-expand col-xl-8').addClass('col-xl-4');
        $(columnActive).removeClass('col-xl-4').addClass('is-result-expand col-xl-8');
      } else {
        return true;
      }

      return false;
    });
  }
}

function modal() {
  $('a[data-toggle="modal"]').click(e => {
    e.preventDefault();
    const target = $(e.currentTarget).attr('data-target');
    $(target).modal('show');
  });

  $('.modal').on('shown.bs.modal', function(e) {
    const modal = $(this);
    const filterGroup = $(modal).find('.filter-group')[0];
    if (filterGroup) {
      const filter             = $(filterGroup).find('.filter')[0];
      const filterButtonActive = $(filterGroup).find('.button-tag.active')[0];
      const indexActive        = $(filterButtonActive).attr('data-filter');
      const paneActive         = $(filter).find(indexActive)[0];
      $(filter).css({
        'height': paneActive.offsetHeight
      });
    }
  });
}

function toggleElement() {
  let btnToggle = $('[data-toggle="toggle-element"]');
  btnToggle.click(e => {
    e.preventDefault;
    const target = $(e.currentTarget).attr('data-target');
    const targetEl = $(target);
    if (targetEl.length > 0) {
      targetEl.toggle(200);
    }
  });
}

function chip(callback) {
  let $chipDel = $('.chip-delete');
  $chipDel.click(e => {
    e.preventDefault();
    const parent = $(e.currentTarget).closest('.chip');
    $(parent).remove();

    // Call the callback
    if (typeof callback == "function") callback();
    return false;
  });
}

function filterTag() {
  let $filterItem = $('.js-filter-tag-group .filter-tag-list .checkbox');
  $('.js-filter-tag-group .filter-tag-list').each((index, list) => countFilterGroup(list));
  dropdownDelete();
  clearFilter();
  autoAddChip();

  $filterItem.click(e => {
    e.preventDefault();
    const filterGroup = $(e.currentTarget).closest('.js-filter-tag-group');
    const filterChecklist = $(filterGroup).find('.filter-checklist')[0];
    const filterTagList = $(e.currentTarget).closest('.filter-tag-list');

    const $checkbox = $(e.currentTarget);
    const input     = $checkbox.find('input[type = "checkbox"]')[0];
    const value     = $(input).val();
    const id        = $checkbox.attr('data-id');
    let isChecked   = $(input).prop('checked');

    if (filterChecklist) {
      if (value.length > 0) {
        $(input).prop('checked', !isChecked);
        !isChecked ? addChip(value, id, filterChecklist) : deleteChip(id, filterChecklist);
        filterList(filterGroup, value);
      }
    }

    if (filterTagList) {
      countFilterGroup(filterTagList, e.currentTarget);
    }

    return false;
  });

  function addChip(value, id, list) {
    let chipHtml = `
      <div class="chip" data-id="${id}">
        <div class="chip-text">${value}</div>
        <button class="chip-delete button-icon" type="button"><span class="ic-close"></span></button>
      </div>
    `;
    $(chipHtml).appendTo($(list));
    chip(unCheckbox(id));
    return false;
  }

  function deleteChip(id, list) {
    const chip = $(list).find('[data-id=' + id + ']')[0];
    if (chip) {
      $(chip).remove();
    }
    return false;
  }

  function unCheckbox(id) {
    const chip     = $('.chip[data-id= "' + id + '"]')[0];
    const btnDel   = $(chip).find('.chip-delete')[0];
    const checkbox = $('.checkbox[data-id = "' + id + '"]')[0];

    $(btnDel).click(e => {
      const input = $(checkbox).find('input[type="checkbox"]')[0];
      const filterGroup = $(checkbox).closest('.js-filter-tag-group');
      const filterTagList = $(filterGroup).find('.filter-tag-list');

      $(input).prop('checked', false);
      if (filterGroup) filterList(filterGroup, input.value);
      if (filterTagList) countFilterGroup(filterTagList, input);

      return false;
    });
  }

  function filterList(filterGroup, value) {
    const inputList = $(filterGroup).find('input[name="filter-list"]')[0];
    if (inputList) {
      let filterListing = inputList.value.split(',');
      let isFilterExist = filterListing.includes(value);
      if (!isFilterExist) {
        filterListing.push(value);
        filterListing.remove('');
        inputList.value = filterListing.join(',');
      } else {
        filterListing.remove(value);
        filterListing.remove('');
        inputList.value = filterListing.join(',');
      }
    }
  }

  function countFilterGroup(list, filterItem) {
    const checkedItems = $(list).find('input[type="checkbox"]:checked');
    const count = checkedItems.length;
    const groupNameArea = $(filterItem).closest('.dropdown').find('.dropdown__button')[0] || $(list).closest('.dropdown').find('.dropdown__button')[0];
    const groupName = $(groupNameArea).text().trim();
    let newName;

    if (count > 0) {
      newName = groupName.split(' (')[0] + ' (' + count + ')';
      $(groupNameArea).addClass('is-active');
      const closeHtml = `
        <span class="button-icon dropdown-delete" type="button" data-name="deleteButton">
          <span class="ic-close"></span>
        </span>
      `;
      const arrow = $(groupNameArea).find('[class^="ic-caret-"]');
      const close = $(groupNameArea).find('.dropdown-delete')[0];
      // $(arrow).hide();
      // if (!close) $(groupNameArea).append($(closeHtml));
      dropdownDelete();
    } else {
      newName = groupName.split(' (')[0];
      $(groupNameArea).removeClass('is-active');
      const arrow = $(groupNameArea).find('[class^="ic-caret-"]');
      const close = $(groupNameArea).find('.dropdown-delete')[0];
      // $(arrow).show();
      // if (close) $(close).remove();
    }
    groupNameArea.firstChild.nodeValue = newName;
  }

  function dropdownDelete() {
    $(document).on('click', '.button-delete', e => {
      e.preventDefault();
      e.stopPropagation();

      const filterActiveItems = $(dropdownButton).closest('.dropdown').find('.dropdown__body input:checked');
      $(filterActiveItems).each((index, item) => {
        $(item).prop('checked', false);
      });
    });
  }

  function clearFilter() {
    let $button = $('.js-filter-tag-group .button-reset');
    $button.on('click', e => {
      e.preventDefault();
      const filterGroup = $(e.currentTarget).closest('.js-filter-tag-group');

      //=== 1. Clear checked inputs
      $(filterGroup).find('input:checked').prop('checked', false);
      $(filterGroup).find('input[type="checkbox"]').removeAttr('checked');

      //=== 2. Clear count and status group
      const dropdown = $(e.currentTarget).closest('.dropdown');
      const dropdownButton = $(dropdown).find('.dropdown__button')[0];
      const groupName = $(dropdownButton).text().trim();
      $(dropdownButton).removeClass('is-active');
      dropdownButton.firstChild.nodeValue = groupName.split(' (')[0];

      //=== 3. Clear selected chips
      $(filterGroup).find('.filter-checklist').html('');

      //=== 4. Other...
    });
  }

  function autoAddChip() {
    const filterGroup = $('.js-filter-tag-group');
    const filterChecklist = $(filterGroup).find('.filter-checklist')[0];
    const inputCheckeds = $(filterGroup).find('input[type="checkbox"]:checked');

    $(inputCheckeds).each((index, input) => {
      const value = input.value;
      const id = $(input).closest('.checkbox').attr('data-id');
      addChip(value, id, filterChecklist);
    });
  }
}

function handleCollapse() {
  // let toggle = $('.collapse');
  // if (toggle.length > 0) {
  //   $(toggle).collapse({
  //     toggle: false
  //   });
  // }

  let btnToggle = $('.js-toggle-collapse');
  $(btnToggle).click(e => {
    e.stopPropagation();
    e.preventDefault();
    const btn         = $(e.currentTarget);
    const target      = $(btn).attr('data-target');
    const text        = $(btn).attr('data-text-collapse');
    const currentText = $(btn).text();
    const status      = $(btn).attr('aria-expanded');

    if (status == 'true') {
      $(target).collapse('hide');
      $(btn).attr('aria-expanded', false);
      toggleText();
    } else {
      $(target).collapse('show');
      $(btn).attr('aria-expanded', true);
      toggleText();
    }

    return false;

    function toggleText() {
      $(btn).text(text);
      $(btn).attr('data-text-collapse', currentText);
    }
  });

  // Only work for accordion ticket
  let multipeCollapseEl = $('.multi-collapse.accordion-body');
  $(multipeCollapseEl).each((index, el) => {
    const accordion = $(el).parent();
    const header = $(accordion).find('.accordion-header')[0];
    $(header).click(e => {
      const accordionListing = accordion.parent();
      const accordionBody = accordion.prevObject;

      // Find all element has same order with accordion
      const list = $(accordionListing[0]).find('.accordion-body.show');
      list.map((index, item) => $(item).collapse('hide'));

      // Scroll to top off collapse body
      setTimeout(() => {
        $('html, body').animate({
          scrollTop: $(window).scrollTop()
        }, 300);
      }, 100);
    });
  });
}

function handleCopy() {
  let btnCopy = $('.card-copy');
  $(btnCopy).click(e => {
    e.preventDefault();
    const card = $(e.currentTarget);
    const data = $(card).attr('data-copy');
    const list = $(card).closest('.copy-list');

    if (data) {
      // Copy to clipboard
      navigator.clipboard.writeText(data).then(() => {
        console.log('Copying to clipboard was successful!');
        if (list.length > 0) $(list).find('.is-active').removeClass('is-active');
        $(card).addClass('is-active');
      }, err => {
        console.error('Could not copy text: ', data);
      });
    }
  });
}

function handleToggleMultipleElements() {
  let method = $('.js-toggle');
  $(method).on('click', e => {
    e.preventDefault();
    console.log(e);
    const list = $(e.currentTarget).closest('.method-list');
    const toggle = $(e.currentTarget).attr('data-toggle');
    const control = $(e.currentTarget).attr('aria-controls');
    const controlGroup = $(e.currentTarget).attr('aria-coltrol-group');

    console.log(controlGroup);

    $(list).find('.js-toggle.is-active').removeClass('is-active');
    $(document).find(controlGroup).collapse('hide');
    $(e.currentTarget).addClass('is-active');
    if (toggle.length == 0) {
      $(control).collapse('hide');
    }
  });
}

function basicScrollTop() {
  var btnTop = document.querySelector('#scroll-top');

  // Reveal the button
  var btnReveal = function () {
    if (window.scrollY >= 300) {
      btnTop.classList.add('is-visible');
    } else {
      btnTop.classList.remove('is-visible');
    }
  }

  // Smooth scroll top
  var TopscrollTo = function () {
    if(window.scrollY!=0) {
      setTimeout(function() {
        window.scrollTo(0,window.scrollY-30);
        TopscrollTo();
      }, 5);
    }
  }
  // Listeners
  window.addEventListener('scroll', btnReveal);
  btnTop.addEventListener('click', TopscrollTo);

};

function handleAutocompleteAccordion() {
  let inputs = $('.accordion input, .accordion select, .accordion textarea');
  inputs.each((index, input) => {
    const targetValue = $(input).attr('data-target-value');
    const accordion = $(input).closest('.accordion');
    const accordionHeader = $(accordion).find('.accordion-header')[0];
    if (accordionHeader) {
      const fillArea = $(accordionHeader).find(`[data-type="${targetValue}"]`)[0];
      _fillText(input, fillArea); // Auto fill when load page
      $(input).on('change', e => _fillText(e.target, fillArea));
    }
  });

  /*
    Attributes:
      @data-text-suffix: add text after value
      @data-text-prefix: add text before value
      @data-text-replace: replace value by string
  */
  function _fillText(input, fillArea) {
    const checkInputTypes = ['radio', 'checkbox'];
    if ((checkInputTypes.includes(input.type) && $(input).is(':checked')) || input.type !== 'radio') {
      if (input.type === 'checkbox' && !$(input).is(':checked')) {
        $(fillArea).text('');
      } else {
        const value        = input.value;
        const labelSuffix  = $(input).attr('data-text-suffix') || '';
        const labelPrefix  = $(input).attr('data-text-prefix') || '';
        const valueReplace = $(input).attr('data-text-replace');
        const resultText   = valueReplace && valueReplace.length > 0
          ? valueReplace
          : value.length > 0 ? [labelPrefix, value, labelSuffix].join(' ').trim() : '';
        $(fillArea).text(resultText);
      }
    }
  }
}

function rating() {
  let rating = $('.rating-star__item');
  if (rating.length > 0) {
    rating.each((index, item) => {
      const input = $(item).find('input[type="checkbox"]')[0];
      _toggleStar(input);
      $(input).on('change', e => {
        _toggleStar(e.target);
      });
    });
  }

  function _toggleStar(input) {
    const ratingItem = $(input).closest('.rating-star__item');
    const isChecked = $(input).prop('checked');
    isChecked ? $(ratingItem).addClass('active') : $(ratingItem).removeClass('active');
  }
}

function handleTriggerInputGroup() {
  $('body').on('click', '.input-group', (e) => {
    e.preventDefault();
    const group = e.currentTarget;
    const input = $(group).find('.input-group-control')[0];
    if ($(input).prop('tagname') != 'SELECT') {
      input.click();
      $(input).focus();
    } else {
      input.click();
      $(input).trigger('mousedown');
    }

    return false;
  });
}

function handleCardTotalHotelDetail() {
  let rooms = $('.js-room');
  let unit = window.roomUnit || 'rooms';
  let serviceListText = window.serviceListText || 'Service listing';
  let services = window.hotelServices || [];
  const form = $('.form-search-hotel')[0];

  if (rooms.length > 0) {
    localStorage.setItem('selectedRooms', JSON.stringify([]));
    localStorage.setItem('selectedRoomsCount', JSON.stringify(0));
  }

  if (form) {
    var countAdult    = $(form).find('[name = "search-hotel-adult"] option:selected').val();
    var countChildren = $(form).find('[name = "search-hotel-children"] option:selected').val();
    // var countBaby     = $(form).find('[name = "search-hotel-baby"] option:selected').val();
  } else {
    console.error('Không tìm thấy form-search-hotel. Vui lòng kiểm tra lại.');
    return false;
  }

  $('.js-room .input-group-number-2 .input-select > div').on('click', function () {
    const room             = $(this).closest('.js-room');
    let selectedRooms      = JSON.parse(localStorage.getItem('selectedRooms'));
    let selectedRoomsCount = parseInt(JSON.parse(localStorage.getItem('selectedRoomsCount')));
    let listingRooms       = [];

    for (let item of selectedRooms) {
      listingRooms.push(...item.rooms);
    }

    const name       = $(room).attr('data-room-name');
    const price      = parseInt($(room).attr('data-price'));
    const key        = $(room).attr('data-key'); // key of room
    const count      = parseInt($(room).find('.input-group-control')[0].value);
    const groupId    = $(room).attr('data-group-id');
    const groupName  = $(room).attr('data-group-name');
    const roomExist  = _checkRoomInSelected(key);
    let isGroupExist = false;

    let cardTotal    = $('.js-card-total')[0];
    let listPos      = $(cardTotal).find('.card-total-list')[0];
    let totalPos     = $(cardTotal).find('.card-total-price > span')[0];
    let tableListing = $(cardTotal).find('.card-total-table-listing')[0];

    let newRoom = {
      name: name,
      price: price,
      key: key,
      count: count
    };
    let groupRooms;
    let indexGroup;

    // Reset listing display
    _resetHtml();

    // Check group exist in localStorage
    if (selectedRooms.length > 0) {
      for (let i = 0; i < selectedRooms.length; i++) {
        const item = selectedRooms[i];
        if (item.id == groupId) {
          isGroupExist = true;
          groupRooms = item;
          indexGroup = i;
        } else {
          isGroupExist = false;
          groupRooms = {
            id: groupId,
            name: groupName,
            rooms: []
          }
        }
      }
    } else {
      isGroupExist = false;
      groupRooms = {
        id: groupId,
        name: groupName,
        rooms: []
      }
    }

    /* Check group and room */

    // If room not exist, create new
    if (!roomExist) {
      listingRooms.push(newRoom);
      groupRooms.rooms.push(newRoom);
      selectedRoomsCount = selectedRoomsCount + newRoom.count;

      if (isGroupExist) {
        selectedRooms[indexGroup] = groupRooms;
      } else {
        selectedRooms.push(groupRooms);
      }

      _setToLocalStorage(selectedRooms, selectedRoomsCount);
    // Else: Update this room
    } else {
      let index = listingRooms.findIndex(x => x.key == roomExist.key);
      let indexInGroup = groupRooms.rooms.findIndex(x => x.key == roomExist.key);

      selectedRoomsCount = selectedRoomsCount - listingRooms[index].count + newRoom.count;
      groupRooms.rooms[indexInGroup] = newRoom;

      if (count == 0) {
        listingRooms.splice(index);
        groupRooms.rooms.length > 1
          ? selectedRooms[indexGroup].rooms.splice(indexInGroup, 1)
          : selectedRooms.splice(indexGroup, 1);
      } else {
        listingRooms[index] = newRoom;
        selectedRooms[indexGroup] = groupRooms;
      }

      _setToLocalStorage(selectedRooms, selectedRoomsCount);
    }

    /* Check and set total price */

    let total = _totalPrice(listingRooms);
    let totalServicePrice = _totalServicePrice();

    if (total > 0) {
      let totalAll = total + totalServicePrice;

      // Display price
      let f = formatedPrice(totalAll);
      $(totalPos).text(f);

      // Display list
      let listHtml = '';
      listingRooms.forEach((item, index) => {
        if (index < 2) {
          let truncat = '';
          if (index == 1) truncat = '...';
          listHtml += `<span>${item.count} ${unit} - ${item.name}${truncat}</span>`;
        }
      });

      $(listPos).html(listHtml);
      _setTableValue(selectedRooms);
      $(cardTotal).slideDown(200);
    } else {
      _resetHtml();
      $(cardTotal).slideUp(300);
    }

    function _checkRoomInSelected(key) {
      return listingRooms.find(roomItem => roomItem.key == key);
    }

    function _setToLocalStorage(selectedRooms, selectedRoomsCount) {
      localStorage.setItem('selectedRooms', JSON.stringify(selectedRooms));
      localStorage.setItem('selectedRoomsCount', selectedRoomsCount);
    }

    function _totalPrice(rooms) {
      return rooms.length >= 1
        ? rooms.reduce((a, b) => a + b.price * b.count, 0)
        : 0;
    }

    function _resetHtml() {
      $(listPos).html('');
      $(totalPos).html('0');
      $(tableListing).find('tbody tr:not(.fixed-row)').remove();
    }

    function _setTableValue(selectedRooms) {
      let html = '';
      let services = {name: serviceListText, list: []};
      for (let group of selectedRooms) html += _tableRowHtml(group.name, group.rooms);
      for (let service of hotelServices) {
        services.list.push({
          name: service.discountText,
          count: service.service,
          price: parseFloat(service.pricePerPerson),
        });
      }
      html += _tableRowHtml(services.name, services.list);

      const area = $(tableListing).find('tbody')[0];
      $(area).append(html);
    }

    function _tableRowHtml(title,rooms) {
      let roomsHtml = '';
      for (let room of rooms) {
        let roomCountText = '';
        let unitText = '';
        if (parseFloat(room.count)) {
          roomCountText = `(x${room.count})`;
          unitText = unit;
        }
        roomsHtml += `<tr>
          <td><div class="time-flight"><span>${room.count} ${unitText}</span><span>${room.name}</span></div></td>
          <td class="text-right">${roomCountText} ${formatedPrice(room.price)}</td>
        </tr>`;
      }
      const rowTitle = `
        <tr>
          <td><span class="table-title text-uppercase">${title}</span></td>
          <td class="text-right">&nbsp;</td>
        </tr>
      `;
      const rowInfo = `
        <tr>
          <td colspan="2" class="p-0">
            <table class="w-100 table-mul-class">
              <tbody>${roomsHtml}</tbody>
            </table>
          </td>
        </tr>
      `;
      const rowDevider = `
        <tr>
          <td colspan="2">
            <div class="devider"></div>
          </td>
        </tr>
      `;
      return rowTitle + rowInfo + rowDevider;
    }


  });

  function _totalServicePrice() {
    let totalPrice = 0;
    for (let i = 0; i < hotelServices.length; i++) {
      const service = hotelServices[i];
      let pricePerPerson      = parseFloat(service.pricePerPerson);
      let discountPerChildren = parseFloat(service.discountPerChildren) / 100;
      let discountPerPerson   = parseFloat(service.discountPerPerson) / 100;
      totalPrice
        += (pricePerPerson * countAdult * (1 - discountPerPerson))
        + (pricePerPerson * countChildren * (1 - discountPerChildren));
    }
    return totalPrice;
  }

  $('.js-card-total .show-more').on('click', e => {
    e.preventDefault();
    const card = $(e.target).closest('.js-card-total');
    $(card).toggleClass('is-expand');
  });
}

function toggleHotelRooms() {
  let roomType = $('.js-room-type');
  if (roomType.length > 0) {
    $('.js-room-type .room-list .js-btn-toggle a.link').on('click', function (e) {
      e.preventDefault();
      $(this).closest('.js-room-type').find('.multi-collapse').slideToggle();
      $(this).hide();
      $(this).siblings().show();
    })
  }
}

function handleHiddenWhenSticky() {
  const stickyEls = document.querySelector('.is-sticky');
  const hiddenEl = $(stickyEls).find('.hidden-on-sticky')[0];
  let headroom = new Headroom(stickyEls, {
    offset: 400,
    onTop: () => {
    },
    onNotTop: _activeHiddenEls()
  });
  headroom.init();

  function _activeHiddenEls() {
    if (hiddenEl) {
      $(stickyEls).mouseover(e => {
        $(hiddenEl).show(100);
      }).mouseleave(e => {
        $(hiddenEl).hide(100);
      });
    }
  }
}

function handleToggleCheckbox() {
  const checkbox = $('.js-checkbox-collapse input');

  if (checkbox.length > 0) {
    checkbox.on('change', function (e) {
      const target = $(this).parent().data('target');
      $(`[data-checkbox-target=${target}]`).slideToggle();
    })
  }
}

function handleTextMore() {
  let textExpander = $('.js-text-expander');
  if (textExpander.length > 0) {
    let btnShow = $('.js-text-expander .js-btn-show');
    btnShow.on('click', (e) => {
      e.preventDefault();
      let el = $(e.target);
      let btnHide1 = el.siblings('.js-btn-hide');
      let textWrapper = $(el).closest('.js-text-expander').find('.text-expander');
      textWrapper.css('display', 'block');
      textWrapper.addClass('open');
      btnShow.hide();
      btnHide1.show();
    })

    let btnHide = $('.js-text-expander .js-btn-hide');
    btnHide.on('click', (e) => {
      e.preventDefault();
      let el = $(e.target);
      let btnShow1 = el.siblings('.js-btn-show');
      let textWrapper = $(el).closest('.js-text-expander').find('.text-expander');
      textWrapper.css('display', 'block');
      textWrapper.removeClass('open');
      setTimeout(() => {
        textWrapper.css('display', '-webkit-box');
      }, 520);
      btnShow1.show();
      btnHide.hide();
    })
  }
}

function handleSliderAbout() {
  let sliderAbout = $('.slider-about');
  if (sliderAbout.length > 0) {
    sliderAbout.each((index, el) => {
      let min = parseInt(el.dataset.min);
      let max = parseInt(el.dataset.max);
      let stepValue = parseInt(el.dataset.step);
      let valueMin = $(el).find('.slider-about__min span');
      let valueMax = $(el).find('.slider-about__max span');
      let numberMin = $(el).find('input[type="number"].slider-about__min');
      let numberMax = $(el).find('input[type="number"].slider-about__max');

      noUiSlider.create(el, {
        start: [min, max],
        connect: true,
        step: stepValue,
        range: {
          'min': min,
          'max': max
        },
        format: {
          to: function (value) {
            return value;
          },
          from: function (value) {
            return Number(value);
          }
        }
      });

      el.noUiSlider.on('update', function (values, handle) {
        let value = Math.round(values[handle]);
        let fValue = formatedPrice(value);

        if (numberMin.length) {
          // input type number exists
          handle ? numberMax.val(value) : numberMin.val(value);
        } else {
          handle ? valueMax.text(fValue) : valueMin.text(fValue);
        }
      });

      numberMin.on('input', function() {
        if ($(this).val() === '') {
          $(this).val(0);
        }
        if (parseInt($(this).val()) > parseInt(numberMax.val())) {
          $(this).val(parseInt(numberMax.val()));
        }
        el.noUiSlider.set([parseInt($(this).val()), null]);
      });

      numberMax.on('input', function() {
        if ($(this).val() === '' || (parseInt($(this).val()) < parseInt(numberMin.val()))) {
          $(this).val(parseInt(numberMin.val()));
        }
        if (parseInt($(this).val()) > max) {
          $(this).val(max);
        }
        el.noUiSlider.set([null, parseInt($(this).val())]);
      });
    })
  }
}

function handleModalRoomDetail() {
  const roomInfoList = $('.room-info');
  if (roomInfoList.length > 0) {
    function openModal(el) {
      let initSlick = false;
      const roomInfo = $(el).closest('.room-info');
      const roomInfoNumber = roomInfo.data('room-detail');
      const modalGalleryRoomDetail = $('.modal-gallery-room-detail');
      const sliderGalleryWrapper = modalGalleryRoomDetail.find('.slider-gallery');
      const sliderGalleryNavWrapper = modalGalleryRoomDetail.find('.slider-gallery-nav');
      const roomDetailText = modalGalleryRoomDetail.find('.slider-gallery-room__detail-wrapper');
      $.ajax({
        url: roomInfo.data('json'),
        dataType: 'JSON',
        method: 'GET'
      }).done(function(data) {
        if (modalGalleryRoomDetail.data('room-detail') === undefined) {
          // First time open -> no data
          initSlick = true;
        } else if (modalGalleryRoomDetail.data('room-detail') != roomInfoNumber){
          initSlick = true;
          removeSliderModalGalleryRoomDetail();
        }
        if (initSlick) {
          modalGalleryRoomDetail.data('room-detail', roomInfoNumber)
          sliderGalleryWrapper.empty();
          sliderGalleryNavWrapper.empty();
          roomDetailText.empty();
          data.image.forEach(img => {
            const galleryImg = `
              <div class="gallery-img bg-reset" style="background-image: url('${img.url_full}');">
                <div class="caption">
                  <div class="caption-title"></div>
                  <div class="caption-number"></div>
                </div>
              </div>
            `;
            sliderGalleryWrapper.append(galleryImg);

            const galleryImgNav = `<div class="gallery-img bg-reset" style="background-image: url('${img.url_thumb}');"></div>`;
            sliderGalleryNavWrapper.append(galleryImgNav);
          });

          let roomTextHtml = `
            <h4 class="text-title">${data.name}</h4>
            <div class="gallery-service-wrapper mb-3">`;
          data.attr.forEach(attr => {
            const itemService = `
              <div class="item-service">
                <img src="${attr.icon}"></img><span>${attr.name}</span>
              </div>
            `;
            roomTextHtml += itemService;
          });
          roomTextHtml += '</div>';
          roomDetailText.append(roomTextHtml);

          initSliderModalGalleryRoomDetail();

          setTimeout(() => {
            modalGalleryRoomDetail.modal('show');
          }, 1000);
        } else {
          modalGalleryRoomDetail.modal('show');
        }
      });

      modalGalleryRoomDetail.one('shown.bs.modal', () => {
        if (initSlick) {
          let $sliderGallery = $('.modal-gallery-room-detail .slider-gallery');
          let $sliderGalleryNav = $('.modal-gallery-room-detail .slider-gallery-nav');
          $sliderGallery.slick('setPosition');
          $sliderGalleryNav.slick('setPosition');
          initSlick = false;
        }
      });

      function initSliderModalGalleryRoomDetail() {
        let $sliderGallery = $('.modal-gallery-room-detail .slider-gallery');
        let $sliderGalleryNav = $('.modal-gallery-room-detail .slider-gallery-nav');
        if ($sliderGallery.length > 0) {
          let options = {
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            adaptiveHeight: true,
            infinite: false,
            useTransform: true,
            dots: true,
            cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
            prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
            nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>'
          };

          // Show slide number
          // Init slider
          $sliderGallery.on('init', function (event, slick, currentSlide) {
            // no dots -> no slides
            if(!slick.$dots){
              return;
            }

            // use dots to get some count information
            $sliderGallery.find('.caption-number').text(1 + '/' + (slick.$dots[0].children.length));
          });

          $sliderGallery.slick(options);

          $sliderGalleryNav.on('init', function(event, slick) {
            $(`.modal-gallery-room-detail .slider-gallery-nav .slick-slide[data-slick-index="0"]`).addClass('is-active');
          }).slick({
            slidesToShow: 8.5,
            slidesToScroll: 8.5,
            dots: false,
            focusOnSelect: false,
            infinite: false,
            arrows: false,
            centerPadding: '5px',
          });

          $sliderGallery.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            // no dots -> no slides
            if(!slick.$dots){
              return;
            }

            let i = (currentSlide ? currentSlide : 0) + 1;
            // Show slide number
            $sliderGallery.find('.caption-number').text(i + '/' + (slick.$dots[0].children.length));

            // Handle event on sliderGalleryNav
            $sliderGalleryNav.slick('slickGoTo', currentSlide);
            let currrentNavSlideElem = '.modal-gallery-room-detail .slider-gallery-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
            $('.modal-gallery-room-detail .slider-gallery-nav .slick-slide.is-active').removeClass('is-active');
            $(currrentNavSlideElem).addClass('is-active');
          });

          $sliderGalleryNav.on('click', '.slick-slide', function(e) {
            e.preventDefault();
            var goToSingleSlide = $(this).data('slick-index');

            $sliderGallery.slick('slickGoTo', goToSingleSlide);
          });
        }
      }

      function removeSliderModalGalleryRoomDetail() {
        let $sliderGallery = $('.modal-gallery-room-detail .slider-gallery');
        let $sliderGalleryNav = $('.modal-gallery-room-detail .slider-gallery-nav');
        $sliderGallery.slick('unslick');
        $sliderGalleryNav.slick('unslick');
      }
    }
    roomInfoList.find('a.link').on('click', function (e) {
      e.preventDefault();
      openModal(this);
    });
    roomInfoList.find('.room-info__photo').on('click', function (e) {
      e.preventDefault();
      openModal(this);
    });
  }
}

function handleModalGallery() {
  const cardHotelItem = $('.card-hotel');
  if (cardHotelItem.length > 0) {
    function openModal(el) {
      let initSlick = false;
      const cardHotel = $(el).closest('.card-hotel');
      const cardHotelNumber = cardHotel.data('hotel');
      const modalGallery = $('.modal-gallery');
      const sliderGalleryWrapper = modalGallery.find('.slider-gallery');
      const sliderGalleryNavWrapper = modalGallery.find('.slider-gallery-nav');
      const hotelText1 = modalGallery.find('.gallery-service-wrapper');
      const hotelText2 = modalGallery.find('.gallery-location');
      const jsLoading = $(modalGallery).find('.js-loading');
      jsLoading.css('display','flex');

      const imgLoading =  $(el).find('.js-loading');
      imgLoading.css('display','flex');
      
      $.ajax({
        url: cardHotel.data('json'),
        dataType: 'JSON',
        method: 'GET'
      }).done(function(data) {
        if (modalGallery.data('hotel-id') === undefined) {
          // First time open -> no data
          initSlick = true;
        } else if (modalGallery.data('hotel-id') != cardHotelNumber){
          initSlick = true;
          removeSliderModalGallery();
        }
        if (initSlick) {
          modalGallery.data('hotel-id', cardHotelNumber)
          sliderGalleryWrapper.empty();
          sliderGalleryNavWrapper.empty();
          hotelText1.empty();
          hotelText2.empty();
          data.image.forEach(img => {
            const galleryImg = `
              <div class="gallery-img bg-reset" style="background-image: url('${img.url_full}');">
                <div class="caption">
                  <div class="caption-title"></div>
                  <div class="caption-number"></div>
                </div>
              </div>
            `;
            sliderGalleryWrapper.append(galleryImg);

            const galleryImgNav = `<div class="gallery-img bg-reset" style="background-image: url('${img.url_thumb}');"></div>`;
            sliderGalleryNavWrapper.append(galleryImgNav);
          });

          data.attr.forEach(attr => {
            const hotelTextHtml1 = `
              <div class="item-service">
                <img src="${attr.icon}"></img><span>${attr.name}</span>
              </div>
            `;
            hotelText1.append(hotelTextHtml1);
          });

          data.location_recent.forEach(location_recent => {
            const hotelTextHtml2 = `
              <li>
                <span>${location_recent.location}</span>
                <span>${location_recent.range}</span>
              </li>
            `;
            hotelText2.append(hotelTextHtml2);
          });

          initSliderModalGallery();
          
          imgLoading.fadeOut(1000);
          setTimeout(() => {
            modalGallery.modal('show');
          }, 1000);

        } else {
          imgLoading.fadeOut(1000);
          modalGallery.modal('show');
          jsLoading.fadeOut();
        }
      });

      modalGallery.one('shown.bs.modal', () => {
        if (initSlick) {
          let $sliderGallery = $('.modal-gallery .slider-gallery');
          let $sliderGalleryNav = $('.modal-gallery .slider-gallery-nav');
          let $loading = $('.modal-gallery .js-loading');
          $sliderGallery.slick('setPosition');
          $sliderGalleryNav.slick('setPosition');
          $loading.fadeOut();
          initSlick = false;
        }
      });

      function initSliderModalGallery() {
        let $sliderGallery = $('.modal-gallery .slider-gallery');
        let $sliderGalleryNav = $('.modal-gallery .slider-gallery-nav');
        if ($sliderGallery.length > 0) {
          let options = {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            adaptiveHeight: true,
            infinite: false,
            useTransform: true,
            dots: true,
            cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
            prevArrow: '<button type="button" class="slider__control slider__control--left"><span class="ic-chevron-left"></span></button>',
            nextArrow: '<button type="button" class="slider__control slider__control--right"><span class="ic-chevron-right"></span></button>',
          };

          // Show slide number
          // Init slider
          $sliderGallery.on('init', function (event, slick, currentSlide) {
            // no dots -> no slides
            if(!slick.$dots){
              return;
            }

            // use dots to get some count information
            $sliderGallery.find('.caption-number').text(1 + '/' + (slick.$dots[0].children.length));
          });

          $sliderGallery.slick(options);
         
          $sliderGalleryNav.on('init', function(event, slick) {
            $(`.modal-gallery .slider-gallery-nav .slick-slide[data-slick-index="0"]`).addClass('is-active');
          }).slick({
            lazyLoad: 'progressive',
            slidesToShow: 5.5,
            slidesToScroll: 5.5,
            dots: false,
            focusOnSelect: false,
            infinite: false,
            arrows: false,
            centerPadding: '5px',
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 4.5,
                  slidesToScroll: 4.5,
                }
              }
            ]
          });

          $sliderGallery.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            // no dots -> no slides
            if(!slick.$dots){
              return;
            }

            let i = (currentSlide ? currentSlide : 0) + 1;
            // Show slide number
            $sliderGallery.find('.caption-number').text(i + '/' + (slick.$dots[0].children.length));

            // Handle event on sliderGalleryNav
            $sliderGalleryNav.slick('slickGoTo', currentSlide);
            let currrentNavSlideElem = '.modal-gallery .slider-gallery-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
            $('.modal-gallery .slider-gallery-nav .slick-slide.is-active').removeClass('is-active');
            $(currrentNavSlideElem).addClass('is-active');
          });

          $sliderGalleryNav.on('click', '.slick-slide', function(e) {
            e.preventDefault();
            var goToSingleSlide = $(this).data('slick-index');

            $sliderGallery.slick('slickGoTo', goToSingleSlide);
          });
        }
      }

      function removeSliderModalGallery() {
        let $sliderGallery = $('.modal-gallery .slider-gallery');
        let $sliderGalleryNav = $('.modal-gallery .slider-gallery-nav');
        $sliderGallery.slick('unslick');
        $sliderGalleryNav.slick('unslick');
      }
    }

    cardHotelItem.find('.card-image').on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
      openModal(this);
    });
  }
}

function pickMonthYear() {
  const picker = $('#datepicker-month-year');
  if (picker.length > 0) {
    const input = $(picker).find('input[type="hidden"]')[0];
    let config = {
      inline: true,
      alwaysOpen: true,
      format: 'DD-MM-YYYY',
      language: 'custom',
      singleMonth: true,
      container: picker,
      monthSelect: true,
      yearSelect: true,
      endDate: moment(new Date()).format('DD-MM-YYYY'),
      getValue: () => {
        return $(input).val();
      }
    }
    $(picker).dateRangePicker(config);

    let selects = $(picker).find('.select-wrapper select');
    $(document).on('change', selects, function(e) {
      const _self = $(e.target);
      const isMonth = $(_self).attr('name') == 'month';
      const value = isMonth ? parseInt($(_self).val()) + 1 : $(_self).val();
      const input = $(picker).find('input[type="hidden"]');
      let value2 = '';
      let select2;

      if (isMonth) {
        select2 = $(picker).find('.select-wrapper select[name="year"]');
        value2 = $(select2).val();
        input.val(value + '-' + value2);
      } else {
        select2 = $(picker).find('.select-wrapper select[name="month"]');
        value2 = parseInt($(select2).val()) + 1;
        input.val(value2 + '-' + value);
      }
    });
  }
}
